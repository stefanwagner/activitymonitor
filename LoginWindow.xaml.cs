﻿using caalhp.IcePluginAdapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Diagnostics;


namespace ActivityMonitor
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window 
    {
        XMLservice xmlservice; 
        private AppAdapter _adapter;
        private AcitivityMonitorImplementation _imp;
        
        public Window1()
        {

           // Debugger.Break();

            InitializeComponent();

           //Debugger.Break();
         
            const string endpoint = "localhost";
            try
            {
                _imp = new ActivityMonitor.AcitivityMonitorImplementation(this);
                _adapter = new AppAdapter(endpoint, _imp);
            }
            catch { }

            try
            {
                //xmlservice = new XMLservice();
                //Task.Run(() => { xmlservice = new XMLservice(); }).Start(); 
            }
            catch { }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (checkPatientId(ID2_textbox.Text) == true)
            {
                if (xmlservice == null) xmlservice = new XMLservice();

               
                MainWindow ss = new MainWindow();
                ss.setPatientId(ID2_textbox.Text);
                ss.Show();
                this.Hide();
            }
            
            
        }


         public bool checkPatientId(string patientId)  
        {
            if (patientId == "")  // doesn't check if is is a (valid ID) number
            {
                MessageBox.Show("Please enter the patient's ID number");
            }

            else if (xmlservice.checkIfPatientExcists(patientId) == false) // if patient dont excist in xml file: 
            {
                MessageBox.Show("No maching ID-number was found, please type correct ID, or add new patient!");
            }
            return xmlservice.checkIfPatientExcists(patientId); // returns true if, patient excists in the XML file allready
        }




        public void addNewPatient(string patientId, string name)
        {
            if (patientId != "" && name != "")
            {
                if (xmlservice.checkIfPatientExcists(patientId) == true)
                {
                    MessageBox.Show("Patient with ID " + patientId + " allready excist in database");
                }
                else
                {
                    xmlservice.addPatient(name, patientId);
                    MessageBox.Show("Patient saved to database - Name: " + name + " ID: " + patientId + "");
                }
            }
            else
            {
                MessageBox.Show("Please type both name and ID-number");
            }
        }








        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            if (xmlservice == null) xmlservice = new XMLservice();

            addNewPatient(ID_textbox.Text, name_textbox.Text);
        }
    }
}
