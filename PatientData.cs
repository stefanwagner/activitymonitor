﻿//------------------------------------------------------------------------------
// <auto-oalenerated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18408
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System.Xml.Serialization;
using System.Collections.Generic; 


// 
// This source code was auto-generated by xsd, Version=4.0.30319.33440.
// 


/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
[System.Xml.Serialization.XmlRootAttribute(Namespace="", IsNullable=false)]
public partial class Patients {
    
    
    private List<PatientsPatient> patientField;
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("Patient")]
    //public List<PrøveRegisterPerson> Person
    public List<PatientsPatient> Patient {
        get {
            return this.patientField;
        }
        set {
            this.patientField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
public partial class PatientsPatient {
    
    private string idField;
    
    private string nameField;

    private int goalField;
    
    private List<PatientsPatientMeasurement> measurementsField;
    
    /// <remarks/>
    public string Id {
        get {
            return this.idField;
        }
        set {
            this.idField = value;
        }
    }
    
    /// <remarks/>
    public string Name {
        get {
            return this.nameField;
        }
        set {
            this.nameField = value;
        }
    }


    /// <remarks/>
    public int Goal
    {
        get
        {
            return this.goalField;
        }
        set
        {
            this.goalField = value;
        }
    }
    /// <remarks/>
    [System.Xml.Serialization.XmlArrayItemAttribute("Measurement", IsNullable=false)]
    public List<PatientsPatientMeasurement> Measurements {
        get {
            return this.measurementsField;
        }
        set {
            this.measurementsField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
public partial class PatientsPatientMeasurement {
    
    private System.DateTime dateField;

    private int stepsField;
    
    private int caloriesField;

    private int goalField;
    
    /// <remarks/>
    //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
    public System.DateTime Date {
        get {
            return this.dateField;
        }
        set {
            this.dateField = value;
        }
    }
    
    /// <remarks/>
    
    
    /// <remarks/>
   
    
    /// <remarks/>
    public int Steps {
        get {
            return this.stepsField;
        }
        set {
            this.stepsField = value;
        }
    }

    /// <remarks/>
    public int Goal
    {
        get
        {
            return this.goalField;
        }
        set
        {
            this.goalField = value;
        }
    }
    /// <remarks/>
    public int Calories {
        get {
            return this.caloriesField;
        }
        set {
            this.caloriesField = value;
        }
    }
}
