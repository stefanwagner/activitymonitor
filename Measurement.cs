﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActivityMonitor
{
    class Measurement
    {
        //Year, Month, Day, Step, Active Step, Calories, EX
        //Active Step and EX are ignored as they are not used
        
        public System.DateTime Date { get; set; }
        public int Steps { get; set; }
        public int Calories { get; set; }
        //public int Goal { get; set; }



    }
}
