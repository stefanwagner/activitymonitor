% Alvaro Grande Villarrubia
% Sarcoma Project
% 18-03-2014


file='shimmer178steps.dat';
Array=importdata(file);



Seconds_10=floor(length(Array.data(:,3)) / 512);
 

 


 array_10seg=zeros(Seconds_10,512);
 increment=0;
 
 for j= 1:Seconds_10
     
    for i= 1:512
      
        array_10seg(j,i) = Array.data(i+increment,3) ;
        
    end 
    
    increment= increment+512;
    
 end 

array_mean_acc=zeros(Seconds_10);
array_steps_10s=zeros(Seconds_10);
total_steps=0;

max_value_array=max(array_10seg);
max_value=max(max_value_array);
threshold =2700;
time_slow=0;
time_walking=0;
time_running=0;

for k= 1:Seconds_10
    
     mean_acc=mean(array_10seg(k,:));
     array_mean_acc(k)=mean_acc;
     
    active_steps=0;
    pks=findpeaks(array_10seg(k,:)) ;
    pks=pks';
    n_peaks=length(pks);
    i=0;
    
    for i= 1:n_peaks
        if pks(i)> threshold
            active_steps=active_steps +1 ;
        end
    end
    
    if active_steps<=5
        
        time_slow=time_slow+1;
    end

    if active_steps>5 && active_steps<=15
        
        time_walking=time_walking+1;
    end
     if active_steps>15
        
        time_running=time_running+1;
    end
    
    
    array_steps_10s(k)= active_steps ;
    total_steps= total_steps+active_steps;
    
end

time_slow_sec=time_slow*10;

time_walking_sec=time_walking*10;

time_running_sec=time_running*10;

 slow_walking=linspace(5,5,Seconds_10+2);
 walking=linspace(15,15,Seconds_10+2); 
 running=linspace(25,25,Seconds_10+2); 
 
 time=[time_running_sec 0 0;0 time_walking_sec  0 ; 0 0  time_slow_sec];
 
 total_time_sec=time_slow_sec+time_walking_sec+time_running_sec;
 
 subplot(2,1,1);
 
 

 plot(slow_walking,'b','linewidth',2);
 xlim([1,Seconds_10+2]);
 hold on;
 plot(walking,'y','linewidth',2);
 xlim([1,Seconds_10+2]);
 hold on;
 plot(running,'r','linewidth',2);
 xlim([1,Seconds_10+2]);
 hold on;

 bar(array_steps_10s(:,1),'g');
 xlim([1,Seconds_10+2]);
 ylabel('steps');
 xlabel('time in seconds');
 title('Steps');
 legend('slow walking','walking','running','location','NorthEastOutside');
 hold on;
 
 subplot(2,1,2);

 plot(array_mean_acc(:,1),'b-o','linewidth',3,'markersize',6);
 xlim([1,Seconds_10+2]);
 ylabel('Acceleration');
 xlabel('Time in seconds');
 title('Acceleration');
 
 
 
 circular= [time_running_sec time_walking_sec  time_slow_sec];
 figure;
 
 subplot(1,2,1);
 
 pie(circular);
 labels = {'Running','Walking','Slow Walking'};
 legend(labels,'Location','southoutside','Orientation','horizontal')
 colormap('autumn');
 
 subplot(1,2,2);
 
 bar(time,'group');
 
 ylim([0,total_time_sec]);
 hold off;
 
 
        
 save data.dat total_steps time_running_sec time_walking_sec time_slow_sec -ascii -single