﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActivityMonitor
{
    static class MeasurementService
    {


        //checking if the data transfer is done 
        public static bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }


        //reading the file
        public static List<Measurement> ReadFile(string filepath)
        {
            try
            {
                while (IsFileLocked(new FileInfo(filepath))) { }
                var lines = File.ReadAllLines(filepath);

                var data = from l in lines
                           let split = l.Split(',')
                           select new Measurement
                           {
                               // Skip variable number 5 and 7 as they are not used for anything.
                               //Date =  int.Parse(split[0]) + int.Parse(split[1]) + int.Parse(split[2]),
                               Date = new System.DateTime(int.Parse(split[0]), int.Parse(split[1]), int.Parse(split[2])),
                               Steps = int.Parse(split[3]),
                               Calories = int.Parse(split[5])
                           };

                return data.ToList();
            } catch {

                return null;

            }
    }


    }
}
