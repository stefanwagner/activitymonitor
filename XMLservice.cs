﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Net.Sockets;
using System.Threading;


namespace ActivityMonitor
{
      class XMLservice
    {
        //int patientId;

        private string startupPath;// = Environment.CurrentDirectory;
        private Patients patients = new Patients();
        private List<Measurement> measurementList = new List<Measurement>();

        

        public XMLservice()
        {
            try
            {
                startupPath = Directory.GetDirectories(Directory.GetCurrentDirectory(), "patientdata", SearchOption.AllDirectories)[0];
                loadXMLFile();
            }
            catch { }
        }


        // checks if the patient is in the XML register
        public bool checkIfPatientExcists(string patientId)
        {
        
            bool patientfound = false;
            foreach (PatientsPatient patient in patients.Patient)
            {
                if (patient.Id == patientId)
                {
                    patientfound = true;
                } 
            }
            return patientfound;
        }

        public string getPatientName(string patientId)
        {
        
            string patientName = "";
            foreach (PatientsPatient patient in patients.Patient)
            {
                if (patient.Id == patientId)
                {
                    patientName = patient.Name;
                }
            }
            return patientName;
        }




        // checks if the patient is in the XML register for the login window
        public bool checkIfPatientExcistsLOGIN(string patientId, string name_for_login)
        {
         
            bool patientfound = false;
            foreach (PatientsPatient patient in patients.Patient)
            {
                if (patient.Id == patientId && name_for_login==patient.Name)
                {
                    patientfound = true;
                }
            }
            return patientfound;
        }


        public void setGoal(string patientId, int goal) {
            foreach (PatientsPatient patient in patients.Patient)
            {
                if (patient.Id == patientId)
                {
                    patient.Goal = goal;
                }
            }
            saveXMLFile();
            loadXMLFile();
        }


        public void saveMeasurementsToXML(string patientId, List<Measurement> measurementList)
        {
                foreach(PatientsPatient patient in patients.Patient) 
                {
                    if (patient.Id == patientId)// finds the right patient
                    {
                      foreach (Measurement m in measurementList)
                      {
                          bool excist = false;
                        
                        foreach (PatientsPatientMeasurement xmlMeasurement in patient.Measurements){
                            if (xmlMeasurement.Date == m.Date) {
                                excist = true;
                                if (xmlMeasurement.Steps < m.Steps) {
                                    excist = false;
                                }
                            }
                        }

                        if (excist == false)
                        {
                            PatientsPatientMeasurement pm = new PatientsPatientMeasurement();
                            pm.Date = m.Date;
                            pm.Steps = m.Steps;
                            pm.Calories = m.Calories;
                            pm.Goal = patient.Goal;  // set the goal for this specific measurement, to the present goal set by the doctor
                            patient.Measurements.Add(pm);
                        }

                      }
                    }
                }
            saveXMLFile();
            loadXMLFile();
            } 
        
        

        // Add new patient ot the XML-file
        public void addPatient(string name, string id) {
            PatientsPatient newPatient = new PatientsPatient();

                    newPatient.Name = name;
                    newPatient.Id = id;
                    patients.Patient.Add(newPatient);
                    saveXMLFile();
                    loadXMLFile();
        }


        public List<Measurement> getPatientdata(string patientId){// returns a list of measurements from the XML-file
        
            List<Measurement> measurementlist = new List<Measurement>();
                foreach (PatientsPatient patient in patients.Patient) 
                {
                    if (patient.Id == patientId){
                        foreach (PatientsPatientMeasurement mXml in patient.Measurements) {
                            Measurement mm = new Measurement();

                            mm.Date = mXml.Date;
                            mm.Steps = mXml.Steps;
                            mm.Calories = mXml.Calories;
                            //mm.Goal = mXml.Goal;
                            measurementlist.Add(mm);
                        }

                    }
            }
            return measurementlist;
        }


        // Loads XML file 
        public void loadXMLFile()
        {
            using (FileStream xmlStream = new FileStream(startupPath + "\\PatientData.xml", FileMode.Open))
            {
                using (XmlReader xmlReader = XmlReader.Create(xmlStream))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(Patients));
                    patients = serializer.Deserialize(xmlReader) as Patients;
                }
            }
        }

        public void saveXMLFile()
        {
            using (FileStream xmlStream = new FileStream(startupPath + "\\PatientData.xml", FileMode.Create))
            {
                using (XmlWriter xmlReader = XmlWriter.Create(xmlStream))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(Patients));
                    serializer.Serialize(xmlReader, patients);
                }
            }
        }



    }
}
