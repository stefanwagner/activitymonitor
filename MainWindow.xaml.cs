﻿using System.Collections.Generic;
using System.Windows;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.DataVisualization.Charting;
using caalhp.IcePluginAdapters;
//using Microsoft.Research.DynamicDataDisplay;

namespace ActivityMonitor
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private XMLservice xmlService;  //      <<------------------------------------------------ // XML-service
        private List<Measurement> measurementlist = new List<Measurement>();
        public string patientId;
        //Window1 idWindow = new Window1();
        private AppAdapter _adapter;
        private AcitivityMonitorImplementation _imp;

        public MainWindow()
        {
            InitializeComponent();

            const string endpoint = "localhost";

            try
            {
                _imp = new ActivityMonitor.AcitivityMonitorImplementation(this);
                _adapter = new AppAdapter(endpoint, _imp);
            }
            catch 
            { }

        }

        // Event: Fired when a new measurement file is discovered
        private void FileCreated(object sender, FileSystemEventArgs e)
        {
            Thread.Sleep(200); //<---------------- ;
            FileInfo file = new FileInfo(e.FullPath);
            //wait for the data transfer to be done
            //while (MeasurementService.IsFileLocked(file)) { }
            //then read the measurements
            var measurements = MeasurementService.ReadFile(e.FullPath);
            if (file.Name.Contains("pedometer"))
            {
                measurementlist = measurements;

                UpdateTableAndGraph(measurements);

                //SAves the data to sml-file
                if (patientId != "") { xmlService.saveMeasurementsToXML(patientId, measurementlist); }

            }
        }

        private void UpdateTableAndGraph(List<Measurement> measurements)
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                DataContext = measurements;
                showColumnChart(measurements);
                //LabelMeasurement.Content = "Recent measurements of";
            }));
        }

        private void showColumnChart(List<Measurement> measurements)
        {
            var data = new List<KeyValuePair<string, int>>();
            //var goal = new List<KeyValuePair<int, int>>();
            //Random rnd = new Random();
           // measurements.ForEach(m => data.Add(new KeyValuePair<string, int>(""+m.Date.Day+"/"+m.Date.Month, m.Steps)));
            measurements.ForEach(m => data.Add(new KeyValuePair<string, int>(""+m.Date.Day+"/"+m.Date.Month, m.Steps)));
            //mcChart.Series[0]
            //mcChart.
            //measurements.ForEach(m => goal.Add(new KeyValuePair<int, int>(m.Date.Day, m.Goal)));
            //((ColumnSeries) mcChart.Series[0]).ItemsSource = data;
            ((ColumnSeries)mcChart.Series[0]).ItemsSource = data;
            //((LineSeries) mcChart.Series[1]).ItemsSource = goal;
           // mcChart.U
        }

        //------------------------------------------------ /------------------------------------------------ // 



        public void loadDataFromXML(string patientId)
        {
            if (patientId != "")
            {
                var measurements = xmlService.getPatientdata(patientId);
                Dispatcher.BeginInvoke(new Action(() => { DataContext = measurements; UpdateTableAndGraph(measurements); }));
                //LabelMeasurement.Content = "Saved measurements of";

            }
        }




        public void setGoal(string patientId, int goal)
        {

            xmlService.setGoal(patientId, goal);
            MessageBox.Show("Goal is set to " + goal + " steps for each day!");

        }


        private void Button_Click(object sender, RoutedEventArgs e)
        {
            loadDataFromXML(patientId);      //     <<------------------------------------------------ // XML-service, rename buttons 
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            //MainDataGrid.Visibility = Visibility.Hidden;
            //mcChart.Visibility = Visibility.Visible;

        }

        private void MainDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            //MainDataGrid.Visibility = Visibility.Visible;
            //mcChart.Visibility = Visibility.Hidden;
        }

        private void ButtonSave_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void ButtonSavePatient_Click(object sender, RoutedEventArgs e)
        {

        }

      
        private void Window_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
           //Debugger.Break();

            //Console.WriteLine("Starting Activity Monitor");

            //xmlService = new XMLservice();         //       <<------------------------------------------------ // XML-service                                     
            LoadPatientData();


            //hiding the chart 
            //mcChart.Visibility = Visibility.Hidden;

            // Opens ID-window

            //this.Hide();
            //idWindow.Show();
            try
            {
    

               // Debugger.Break();
                var path = Directory.GetDirectories(Directory.GetCurrentDirectory(), "nfc_com", SearchOption.AllDirectories);
                // Start NFC-communication process
                ProcessStartInfo start = new ProcessStartInfo();
                start.FileName = "NFC-communication.exe";
                Debug.Print(path + "\\nfc_com");
                start.WorkingDirectory = path[0];// +"\\nfc_com";
                start.WindowStyle = ProcessWindowStyle.Hidden;
                start.CreateNoWindow = true;
                Process.Start(start);

                string myWatcherPath = path[0] + "\\Measurements";
                //
                if (!Directory.Exists(myWatcherPath)) Directory.CreateDirectory(myWatcherPath);
                // Check for new measurement files
                FileSystemWatcher myWatcher = new System.IO.FileSystemWatcher();
                myWatcher.Path = myWatcherPath;
                myWatcher.NotifyFilter = NotifyFilters.LastWrite | NotifyFilters.LastAccess | NotifyFilters.FileName;
                myWatcher.Created += new FileSystemEventHandler(FileCreated);
                myWatcher.Filter = "*.csv";
                myWatcher.EnableRaisingEvents = true;
            }
            catch { }
    
        }

        private void LoadPatientData()
        {

            string patientId = "patient1";
            if (xmlService == null)
            {
                xmlService = new XMLservice();
            }
            if (xmlService.checkIfPatientExcists(patientId) == true)
            {
                //MessageBox.Show("Patient with ID " + patientId + " allready excist in database");
                loadDataFromXML(patientId);
            }
            else
            {
                xmlService.addPatient(patientId, patientId);
                //MessageBox.Show("Patient saved to database - Name: " + name + " ID: " + patientId + "");
            }

            this.patientId = patientId;
            //UpdateTableAndGraph(xmlService.
        }




        

    }
}
